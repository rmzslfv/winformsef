﻿namespace PhonebookWindowsFormsEntityFramework.Entities
{
    public class Phone : BaseEntity
    {
        public int ContactID { get; set; }
        public string PhoneNumber { get; set; }
        public virtual Contact Contact { get; set; }
    }
}