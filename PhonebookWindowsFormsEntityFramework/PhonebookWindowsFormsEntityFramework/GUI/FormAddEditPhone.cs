﻿using PhonebookWindowsFormsEntityFramework.Entities;
using System;
using System.Windows.Forms;

namespace PhonebookWindowsFormsEntityFramework.GUI
{
    public partial class FormAddEditPhone : Form
    {
        private Phone phone;

        public FormAddEditPhone(Phone phone)
        {
            InitializeComponent();
            this.phone = phone;
        }

        private void FormAddEditPhone_Load(object sender, EventArgs e)
        {
            this.Text = string.Format("{0} Phone - Phonebook", phone.ID > 0 ? "Update" : "Add");

            textBoxPhoneNumber.Text = phone.PhoneNumber;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            phone.PhoneNumber = textBoxPhoneNumber.Text;

            this.DialogResult = DialogResult.OK;
        }
    }
}
