﻿using PhonebookWindowsFormsEntityFramework.Entities;
using System.Data.Entity;

namespace PhonebookWindowsFormsEntityFramework
{
    public class PhonebookContext : DbContext
    {
        public PhonebookContext() : base("PhonebookDb")
        {
            
        }

        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Phone> Phones { get; set; }
    }
}
